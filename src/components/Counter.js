import {useState} from "react"
const Counter = ({value, index, onCounterChange}) => {
    const increase=()=>{
        onCounterChange(value+1)
    }
    const decrease=()=>{
        onCounterChange(value-1)
    }
    return (
    <div className="counter">
        <button onClick={increase}>+</button>
        <h3>{value}</h3>
        <button onClick={decrease}>-</button>
    </div>
    )
}
export default Counter