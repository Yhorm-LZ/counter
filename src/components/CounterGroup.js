import Counter from "./Counter"

const CounterGroup=(props)=>{
    const{counterList,onChange}=props
    const handleCounterChange=(index,value)=>{
        const updateCounterList = [...counterList]
        updateCounterList[index] = value;
        onChange(updateCounterList)
    }
    return(
        <div>
            {
                counterList.map( (value,index) => <Counter key={index} value={value} index={index} onCounterChange={
                    (value) =>{
                        handleCounterChange(index,value)
                    }
                }></Counter>)
            }
        </div>
    )
}
export default CounterGroup