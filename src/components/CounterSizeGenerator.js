const CounterSizeGenerator=(props)=>{
    const {size,onChange}=props
    const updateSize=(event)=>{
        onChange(Number(event.target.value))
    }
    return (
        <div>
            size: <input type='number' value={size||''} onChange={updateSize}></input>
        </div>
    )
}
export default CounterSizeGenerator