import CounterSizeGenerator from "./CounterSizeGenerator"
import CounterGroup from "./CounterGroup"
import {useState} from "react"
import CounterGroupSum from "./CounterGroupSum";

const MultipleCounter=()=>{
    
    const [counterList,setCounterList]=useState([])

    const handlerCounterList = (newSize) =>{
        setCounterList(Array(newSize).fill(0))
    }

    const sum = counterList.reduce((count,number)=> count+number,0);

    return(
        <div>
            <CounterSizeGenerator size={counterList.length} onChange={handlerCounterList}/>
            <CounterGroupSum sum={sum}></CounterGroupSum>
            <CounterGroup counterList={counterList} onChange={setCounterList}/>
        </div>
    )
}

export default MultipleCounter